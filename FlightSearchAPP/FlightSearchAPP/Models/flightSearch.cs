﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FlightSearchAPP.Models
{
    public class FlightSearch
    {
        public FlightSearchInput FlightSearchInput { get; set; }
        public List<FlightSearchOutputData> FlightSearchOutputData { get; set; }
    }
    public class FlightSearchInput
    {
        [StringLength(3, ErrorMessage = "Please enter valid Departure airport code.", MinimumLength = 3)]
        [Required(ErrorMessage = "Please enter Departure airport code.")]
        public string DepartureAirportCode { get; set; }
        [StringLength(3, ErrorMessage = "Please enter valid Arrival airport code.", MinimumLength = 3)]
        [Required(ErrorMessage = "Please enter Arrival airport code.")]
        public string ArrivalAirportCode { get; set; }

        [DataType(DataType.DateTime)]
        [Required(ErrorMessage = "Please enter valid Departure Date.")]
        public DateTime DepartureDate { get; set; }

        [DataType(DataType.DateTime)]
        [Required(ErrorMessage = "Please enter valid Return Date.")]
        public DateTime ReturnDate { get; set; }
    }
    public class FlightSearchOutputData
    {
        public string AirlineLogo { get; set; }
        public string AirlineName { get; set; }
        public string OutboundFlightDuration { get; set; }
        public string InboundFlightDuration { get; set; }
        public string TotalAmount { get; set; }
    }
}